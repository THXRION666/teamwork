-- encoding = cp1251
script_name("TeamWork")
script_author("THERION")
script_url("https://gitlab.com/THXRION666/teamwork")
local tg_link = "https://t.me/thxr1on"

local CMD = "teamwork"
-- activation command
local MLBUF_SIZE = 1536
-- max team_buffer length. for ~60 team members

-- prints message to SAMP Chat
local function log(msg)
   local format = "{FB4343}[%s]{FFFFFF}: %s{FFFFFF}."
   sampAddChatMessage(string.format(format, thisScript().name, msg), -1)
end

-- prints out if user does not have some of the needed files
local function catch_exception(status, path)
   local msg = string.format('File not found or has errors: "%s". Shutting down..', path)
   assert(status, msg)
end

do -- loading libraries
   local list = {
      lfs        = "lfs",
      ffi        = "ffi",
      inicfg     = "inicfg",
      sampev     = "samp.events",
      imgui      = "mimgui",
      encoding   = "encoding",
      ti         = "tabler_icons",
      style      = string.format("config.%s.style", thisScript().name),
   }

   local result = nil
   for var, path in pairs(list) do
      result, _G[var] = pcall(require, path)
      catch_exception(result, path)
   end
end

--- stuff for imGui
encoding.default = "CP1251"
local u8 = encoding.UTF8
-- imGui window state
local win_state = imgui.new.bool(false)
---

cmds = {} --[[ all commands:
cmd = {
   server = {[0] = string(name), IP1, IP2, ... IPn} or nil, 
   filter = string (file name),
   description = string, 
   func = lua_thread.suspended
}
]]
filters = {} --[[ all filters:
filter = bool function(color, text, team)
]]
config = {} -- config data storage

local filter_to_cmd = {} -- {filter_1 = {command_1, command_2, ... , command_n}, ...}
local team_buffer = nil 
-- imGui multiline text buffer

-- check if array has value
local function is_in_array(array, value)
   for _, element in ipairs(array) do
      if value == element then
         return true
      end
   end
   return false
end

-- multiline string -> one-line string table
local function parse(s)
   local result = {}
   for chunk in string.gmatch(s, "[^\n]+") do
      result[#result + 1] = chunk
   end
   return result
end

-- load every lua module from directory to table
local function load_modules(container_tbl, sub_directory) -- sub_directory = commands | filters | ...
   local result = nil
   local path = string.format("%s\\resource\\%s\\%s", getWorkingDirectory(), thisScript().name, sub_directory)
   for file in lfs.dir(path) do
      local file_name, extension = file:match("(.+)%.(.+)")
      if extension == "lua" then
         local require_path = string.format("resource.%s.%s.%s", thisScript().name, sub_directory, file_name)
         result, _G[container_tbl][file_name] = pcall(require, require_path)
         assert(result, _G[container_tbl][file_name])
      end
   end
end

local function load_filters_and_cmds()
   do -- clear all containers
      cmds = {}
      filters = {}
      filter_to_cmd = {}
   end

   load_modules("cmds", "commands")
   load_modules("filters", "filters")

   local ip, _ = sampGetCurrentServerAddress()
   for name, data in pairs(cmds) do
      -- server check
      if data.server and not is_in_array(data.server, ip) then
         cmds[name].ignore = true
      else
         cmds[name].ignore = false
         filter_to_cmd[data.filter] = filter_to_cmd[data.filter] or {}
         table.insert(filter_to_cmd[data.filter], name)
      end
   end
   -- if filter is unused then it is removed from table
   for name, _ in pairs(filters) do
      if not filter_to_cmd[name] then
         filters[name] = nil
      end
   end
end

-- load config and create settings file if necessary
local function load_cfg(table_name, default, settings)
   local cfg_path = "moonloader\\config\\" .. thisScript().name

   local pattern = "%s\\%s.ini"
   local default_path = string.format(pattern, cfg_path, default)
   if not doesFileExist(default_path) then
      catch_exception(false, default_path)
   end
   local settings_path = string.format(pattern, cfg_path, settings)

   local short_path = string.format(pattern, thisScript().name, settings)
   local short_def = string.format(pattern, thisScript().name, default)
   if not doesFileExist(settings_path) then
      _G[table_name] = inicfg.load(nil, short_def)
      inicfg.save(_G[table_name], short_path)
   else
      _G[table_name] = inicfg.load(nil, short_path)
   end
end
-- create config for new commands
local function init_unknown_cmds()
   -- if new command files are added, set config to false
   for key, _ in pairs(cmds) do
      if config.commands[key] == nil then
         config.commands[key] = false
      end
   end
end

-- refresh/load all lua modules and config files
local function refresh_files()
   load_filters_and_cmds()
   load_cfg("config", "default", "settings")
   init_unknown_cmds()
   imgui.to("config", "team")
end

local function set_chat_hook(bool)
   -- accessing im_bool value from samp events hook causes script crash so:
   sampev.onServerMessage = bool and
   function(color, text)
      for f_name, cmd_list in pairs(filter_to_cmd) do
         if filters[f_name](color, text, config.team) then
            for _, name in ipairs(cmd_list) do
               if config.commands[name][0] then
                  cmds[name].func:run(color, text)
               end
            end
         end
      end
   end or
   function(color, text)
      return {color, text}
   end
end

function main()
   repeat wait(0) until isSampAvailable()

   refresh_files()
   
   set_chat_hook(config.general.on[0])

   sampRegisterChatCommand(CMD, 
   function() 
      win_state[0] = not win_state[0] 
   end)

   wait(-1)
end

local function load_icons(font_size)
   local config = imgui.ImFontConfig()
   config.MergeMode = true
   config.PixelSnapH = true
   local iconRanges = imgui.new.ImWchar[3](ti.min_range, ti.max_range, 0)
   imgui.GetIO().Fonts:AddFontFromMemoryCompressedBase85TTF(ti.get_font_data_base85(), font_size, config, iconRanges)
end

imgui.OnInitialize(function()
   load_icons(15)
   style.apply_style()
   imgui.GetIO().IniFilename = nil
   -- so it does not create any config files
end)

local new_frame = imgui.OnFrame(function() return win_state[0] end,
function(player)
   player.HideCursor = false
   imgui.SetNextWindowPos(imgui.ImVec2(300, 360), imgui.Cond.FirstUseEver)

   imgui.Begin(thisScript().name, win_state, style.win_flags)
      local item_width = 300 -- minimal window width
      do -- calculate item width
         local space_between = 60

         for name, _ in pairs(config.commands) do
            local name_len = imgui.CalcTextSize(name).x
            if name_len > item_width then
               item_width = name_len
            end
         end
         item_width = item_width
      end

      do -- links
         imgui.Link(thisScript().url, ti("brand-gitlab"), style.white, style.orange)
         imgui.SameLine()
         imgui.Link(tg_link, ti("brand-telegram"), style.white, style.blue)

         imgui.SameLine((imgui.GetWindowSize().x - imgui.CalcTextSize(thisScript().name).x) / 2)
         imgui.Text(thisScript().name)
      end

      if imgui.Checkbox("Answer requests", config.general.on) then -- on/off switch
         set_chat_hook(config.general.on[0])
      end
      
      imgui.PushItemWidth(item_width)

      if imgui.CollapsingHeader("Commands", imgui.TreeNodeFlags.DefaultOpen) then
         for name, value in pairs(config.commands) do
            local data = cmds[name]
            
            imgui.Checkbox("##" .. name, value)

            imgui.SameLine()
            imgui.TextColored(data.ignore and style.gray or style.white, name)

            -- hint
            local pattern = "filter: %s\ndescription: %s\nserver: %s"

            local server = data.server and data.server[0] or "-"
            local hint_text = string.format(pattern, data.filter, data.info or "-", server)
            imgui.Hint("##hint" .. name, hint_text, false, true)
         end
      end

      -- team input
      if imgui.CollapsingHeader("Trustworthy ones", imgui.TreeNodeFlags.DefaultOpen) then 
         if imgui.InputTextMultiline("##teambuffer", team_buffer, MLBUF_SIZE) then
            local str = ffi.string(team_buffer)
            config.team = parse(u8:decode(str))
         end
      end

      imgui.PopItemWidth()

      do -- save, restore default and refresh buttons
         local bottom_btn_size = imgui.ImVec2((item_width - 16) / 3, 20)
         if imgui.Button(ti("bookmark") .. " Save", bottom_btn_size) then
            imgui.from("config", "team")
            inicfg.save(config, thisScript().name .. "\\settings.ini")
            imgui.to("config", "team")
         end

         imgui.SameLine()

         if imgui.Button(ti("settings") .. " Default", bottom_btn_size) then
            config = inicfg.load(nil, thisScript().name .. "\\default.ini") 
            init_unknown_cmds()
            imgui.to("config", "team")
         end

         imgui.SameLine()

         if imgui.Button(ti("refresh") .. " Refresh", bottom_btn_size) then
            refresh_files()
         end
      end
   imgui.End()
end)

-- convert data to mimgui formats
imgui.to = function(table_name, ignored_section)
   for section_name, _ in pairs(_G[table_name]) do
      if section_name ~= ignored_section then
         local settings = _G[table_name][section_name]
         for key, value in pairs(settings) do
            local data_type = type(settings[key])
            if data_type == "boolean" then 
               settings[key] = imgui.new.bool(value)
            end
            --[[ BUFFER_SIZE undefined, and not needed
            if data_type == "string" then
               settings[key] = imgui.new.char[BUFFER_SIZE](u8(value))
            end
            ]]
            if data_type == "number" then
               if key:find("f_") then
                  settings[key] = imgui.new.float(value)
               end
               if key:find("i_") then
                  settings[key] = imgui.new.int(value)
               end
               --[[ cause i don't actually need this here
               if key:find("color_") then
                  settings[key] = clr_works.argb_to_mimgui(value)
               end
               ]]
            end
         end
         _G[table_name][section_name] = settings
      end
   end
   --
   team_buffer = imgui.new.char[MLBUF_SIZE](u8(table.concat(config.team, "\n")))
end

-- get original data from mimgui formats
imgui.from = function(table_name, ignored_section)
   for section_name, _ in pairs(_G[table_name]) do
      if section_name ~= ignored_section then 
         local settings = _G[table_name][section_name]
         for key, value in pairs(settings) do
            if settings[key] then
               if type(settings[key]) == "cdata" then
                  local data_type = ffi.typeof(settings[key])
                  if tostring(data_type):match("ctype<(%S+)%s%[1%]>") then
                     settings[key] = settings[key][0]
                  end
               end
            end
         end
         _G[table_name][section_name] = settings
      end
   end
end

imgui.Link = function(link, name, color, color_hovered)
   local size = imgui.CalcTextSize(name)
   local cursor_pos = imgui.GetCursorPos()
   local result_btn = imgui.InvisibleButton("##" .. link .. name, size)

   if result_btn then
      os.execute("explorer ".. link)
   end

   imgui.SetCursorPos(cursor_pos)
   if imgui.IsItemHovered() then
      imgui.TextColored(color_hovered, name)
   else
      imgui.TextColored(color, name)
   end
   
   return result_btn
end

local POOL_HINTS = {}
imgui.Hint = function(str_id, hint_text, color, no_center)
   color = color or imgui.GetStyle().Colors[imgui.Col.PopupBg]
   local p_orig = imgui.GetCursorPos()
   local hovered = imgui.IsItemHovered()
   imgui.SameLine(nil, 0)

   local anim_time = 0.2
   local show = true

   if not POOL_HINTS[str_id] then
      POOL_HINTS[str_id] = {
         status = false,
         timer = 0
      }
   end

   if hovered then
      for k, v in pairs(POOL_HINTS) do
         if k ~= str_id and os.clock() - v.timer <= anim_time then
            show = false
         end
      end
   end

   if show and POOL_HINTS[str_id].status ~= hovered then
      POOL_HINTS[str_id].status = hovered
      POOL_HINTS[str_id].timer = os.clock()
   end

   local getContrastColor = function(col)
      local luminance = 1 - (0.299 * col.x + 0.587 * col.y + 0.114 * col.z)
      return luminance < 0.5 and imgui.ImVec4(0, 0, 0, 1) or imgui.ImVec4(1, 1, 1, 1)
   end

   local rend_window = function(alpha)
      local size = imgui.GetItemRectSize()
      local scrPos = imgui.GetCursorScreenPos()
      local DL = imgui.GetWindowDrawList()
      local center = imgui.ImVec2( scrPos.x - (size.x / 2), scrPos.y + (size.y / 2) - (alpha * 4) + 10 )
      local a = imgui.ImVec2( center.x - 7, center.y - size.y - 3 )
      local b = imgui.ImVec2( center.x + 7, center.y - size.y - 3)
      local c = imgui.ImVec2( center.x, center.y - size.y + 3 )
      local col = imgui.ColorConvertFloat4ToU32(imgui.ImVec4(color.x, color.y, color.z, alpha))

      DL:AddTriangleFilled(a, b, c, col)
      imgui.SetNextWindowPos(imgui.ImVec2(center.x, center.y - size.y - 3), imgui.Cond.Always, imgui.ImVec2(0.5, 1.0))
      imgui.PushStyleColor(imgui.Col.PopupBg, color)
      imgui.PushStyleColor(imgui.Col.Border, color)
      imgui.PushStyleColor(imgui.Col.Text, getContrastColor(color))
      imgui.PushStyleVarVec2(imgui.StyleVar.WindowPadding, imgui.ImVec2(8, 8))
      imgui.PushStyleVarFloat(imgui.StyleVar.WindowRounding, 6)
      imgui.PushStyleVarFloat(imgui.StyleVar.Alpha, alpha)

      local max_width = function(text)
         local result = 0
         for line in text:gmatch('[^\n]+') do
            local len = imgui.CalcTextSize(line).x
            if len > result then
               result = len
            end
         end
         return result
      end

      local hint_width = max_width(hint_text) + (imgui.GetStyle().WindowPadding.x * 2)
      imgui.SetNextWindowSize(imgui.ImVec2(hint_width, -1), imgui.Cond.Always)
      imgui.Begin('##' .. str_id, _, imgui.WindowFlags.Tooltip + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoScrollbar + imgui.WindowFlags.NoTitleBar)
         for line in hint_text:gmatch('[^\n]+') do
            if no_center then
               imgui.Text(line)
            else
               imgui.SetCursorPosX((hint_width - imgui.CalcTextSize(line).x) / 2)
               imgui.Text(line)
            end
         end
      imgui.End()

      imgui.PopStyleVar(3)
      imgui.PopStyleColor(3)
   end

   if show then
      local between = os.clock() - POOL_HINTS[str_id].timer
      if between <= anim_time then
         local s = function(f)
            return f < 0.0 and 0.0 or (f > 1.0 and 1.0 or f)
         end
         local alpha = hovered and s(between / anim_time) or s(1.00 - between / anim_time)
         rend_window(alpha)
      elseif hovered then
         rend_window(1.00)
      end
   end

   imgui.SetCursorPos(p_orig)
end