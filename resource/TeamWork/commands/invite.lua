local cmd = {
   info = "/inv <id/nick> command trigger (team only)",
   server = {[0] = "GALAXY-RPG", "176.32.39.200", "176.32.39.199", "176.32.39.198"},
   filter  = "pm_galaxy",
   func    = nil
}
 
cmd.func = lua_thread.create_suspended(
function(_, text)
   local invited = text:match("/inv (%S+)")
   if invited then
      wait(0)
      sampSendChat(string.format("/invite %s", invited))
   end
end)

return cmd