local cmd = {
   info = "/hl <1 - 6> command trigger (team only)",
   server = {[0] = "GALAXY-RPG", "176.32.39.200", "176.32.39.199", "176.32.39.198"},
   filter  = "pm_galaxy",
   func    = nil
}

cmd.func = lua_thread.create_suspended(
function(_, text)
   local veh_idx = tonumber(text:match("/hl (%d+)")) or 0 -- get the passed number
   if veh_idx >= 1 and veh_idx <= 6 then                  -- check if number is suitable
      wait(0)                                             -- wait() is necessary before accessing SAMP Chat
      sampSendChat(string.format("/hlock %s", veh_idx))   -- send "/hlock <number>" to SAMP Chat
   end
end)

return cmd