local cmd = {
   info = '/kick <id/nick> "<reason>" command trigger (team only)',
   server = {[0] = "GALAXY-RPG", "176.32.39.200", "176.32.39.199", "176.32.39.198"},
   filter  = "pm_galaxy",
   func    = nil
}
local PM_REGEX = ">> �� �� (%S+)%(%d+%):%s.+"

cmd.func = lua_thread.create_suspended(
function(_, text)
   wait(0)
   local res_cmd = nil
   local player, reason = text:match('/kick (%S+)%s(%b"")')
   if player and reason then
      reason = string.gsub(reason, '"', "")
      local nick = text:match(PM_REGEX)
      res_cmd = string.format("/uninvite %s %s (c) %s", player, reason, nick)
   else
      player = text:match("/kick (%S+)")
      if not player then return end
      res_cmd = string.format("/uninvite %s", player)
   end
   sampSendChat(res_cmd)
end)

return cmd