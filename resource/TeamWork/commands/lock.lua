local response = "/lock"

local cmd = {
   info = "/lock command trigger (team only)",
   server = {[0] = "GALAXY-RPG", "176.32.39.200", "176.32.39.199", "176.32.39.198"},
   filter  = "pm_galaxy",
   func    = nil
}
 
cmd.func = lua_thread.create_suspended(
function(_, text)
   if text:find(response) then
      wait(0)                   -- wait() is necessary before accessing SAMP Chat
      sampSendChat(response) -- send "/lock" to SAMP Chat
   end
end)
 
return cmd