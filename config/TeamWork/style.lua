local style = {}
local imgui = require("mimgui")
local vec4 = imgui.ImVec4
local vec2 = imgui.ImVec2

style.win_flags = imgui.WindowFlags.NoResize + imgui.WindowFlags.NoTitleBar + imgui.WindowFlags.AlwaysAutoResize

style.white = imgui.ImVec4(1, 1, 1, 1)
style.orange = imgui.ImVec4(1, 0.32, 0, 1)
style.blue = imgui.ImVec4(0.32, 0.64, 1, 1)
style.gray = imgui.ImVec4(0.5, 0.5, 0.5, 1)

-- imgui window style
style.apply_style = function()
   -- thx man with a moon profile picture
   imgui.SwitchContext()
	local colors = imgui.GetStyle().Colors

   colors[imgui.Col.Text]                    = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.TextDisabled]            = imgui.ImVec4(1.00, 1.00, 1.00, 0.20)
   colors[imgui.Col.PopupBg]                 = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.Border]                  = imgui.ImVec4(0.32, 0.32, 0.32, 0.00)
   colors[imgui.Col.SliderGrab]              = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.SliderGrabActive]        = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.BorderShadow]            = imgui.ImVec4(0.00, 0.00, 0.00, 0.00)
   colors[imgui.Col.ScrollbarBg]             = imgui.ImVec4(0.60, 0.60, 0.60, 0.90)
   colors[imgui.Col.ScrollbarGrab]           = imgui.ImVec4(0.90, 0.90, 0.90, 1.00)
   colors[imgui.Col.ScrollbarGrabHovered]    = imgui.ImVec4(0.80, 0.80, 0.80, 1.00)
   colors[imgui.Col.ScrollbarGrabActive]     = imgui.ImVec4(0.70, 0.70, 0.70, 1.00)
   colors[imgui.Col.FrameBg]                 = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.FrameBgHovered]          = imgui.ImVec4(0.20, 0.20, 0.20, 0.80)
   colors[imgui.Col.FrameBgActive]           = imgui.ImVec4(0.20, 0.20, 0.20, 0.60)
   colors[imgui.Col.CheckMark]               = imgui.ImVec4(1.00, 1.00, 1.00, 1.00)
   colors[imgui.Col.Button]                  = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.ButtonHovered]           = imgui.ImVec4(0.15, 0.15, 0.15, 1.00)
   colors[imgui.Col.ButtonActive]            = imgui.ImVec4(0.10, 0.10, 0.10, 1.00)
   colors[imgui.Col.TextSelectedBg]          = imgui.ImVec4(0.80, 0.80, 0.80, 0.80)
   colors[imgui.Col.TitleBg]                 = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.TitleBgActive]           = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.Header]                  = imgui.ImVec4(0.20, 0.20, 0.20, 1.00)
   colors[imgui.Col.HeaderHovered]           = imgui.ImVec4(0.30, 0.30, 0.30, 1.00)
   colors[imgui.Col.HeaderActive]            = imgui.ImVec4(0.40, 0.40, 0.40, 1.00)
end

return style